import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../database/database';

@Injectable()
export class ProductProvider {

    constructor(private dbProvider: DatabaseProvider) { }

    public insert(product: Product) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            let sql = 'INSERT INTO DADF370 (CODPAU, CODPRO, DESPAU, DESPRO, UNIDAD, IMGPRO, CON001, DES001, DIA001, PRE001, CON002, DES002, DIA002, PRE002, CON003, DES003, DES003, DIA003, PRE003, CON004, DES004, DIA004, PRE004, CON005, DES005, DIA005, PRE005, CON006, DES006, DIA006, PRE006, CON007, DES007, DIA007, PRE007, CON008, DES008, DIA008, PRE008, CON009, DES009, DIA009, PRE009, CON010, DES010, DIA010, PRE010, PARA01, PARA02, PARA03, PARA04, PARA05, REST01, REST02, REST03, RESL01, RESL02, RESL03, RESD01, RESD02, RESD03) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

            let data = ['1', product.CODPRO, '1', product.DESPRO, product.UNIDAD, '', '0', '', '0', product.PRE001, ' ', '0', '0', ' ', '0', '0', ' ', '0', '0', ' ', '0', '0', ' ', '0', '0', '0', '0', '0', '0', '0', '0', '0', ' ', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '0', '0', '0', '0', '0', '0', '', '', ''];


            return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public update(product: Product) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            let sql = 'UPDATE DADF370 SET DESPRO = ?, UNIDAD = ?, IMGPRO = ?, PRE001 = ?, CON010 = ? WHERE CODPRO = ?';

            let data = [
                product.DESPRO,
                product.UNIDAD,
                product.IMGPRO,
                product.PRE001,
                product.CODPRO,
                product.CON010
            ];

            return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public remove(codigo: number) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
            let sql = 'DELETE FROM DADF370 WHERE CODPRO = ?';
            let data = [codigo];

            return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public get(codigo: number) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            let sql  = 'SELECT * FROM DADF370 WHERE CODPRO = ?';
            let data = [codigo];

            return db.executeSql(sql, data)
            .then((data: any) => {
                if (data.rows.length > 0) {
                    let item       = data.rows.item(0);
                    let product    = new Product();

                    product.CODPRO = item.CODPRO;
                    product.DESPRO = item.DESPRO;
                    product.UNIDAD = item.UNIDAD;
                    product.IMGPRO = item.IMGPRO;
                    product.PRE001 = item.PRE001;
                    product.CON010 = item.CON010;

                    return product;
                }

                return null;
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public getAll(descri: string = null) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            let sql = 'SELECT * FROM DADF370 ';

            var data: any[] = [];

            // filtrando pelo nome
            if(descri) {
                sql += ' WHERE DESPRO LIKE ?'
                data.push('%' + descri + '%');
            }

            return db.executeSql(sql, data)
            .then((data: any) => {

                if (data.rows.length > 0) {
                    let products: any[] = [];

                    for (var i = 0; i < data.rows.length; i++) {
                        var product = data.rows.item(i);
                        products.push(product);
                    }
                    return products;
                } else {
                    return [];
                }
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }
}

export class Product {
    CODPRO: number;
    DESPRO: string;
    UNIDAD: string;
    IMGPRO: string;
    PRE001: number;
    CON010: number;
    //duedate: Date;
    //active: boolean;
    //category_id: number;
}
