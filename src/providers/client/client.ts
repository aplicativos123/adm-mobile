import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../database/database';

@Injectable()
export class ClientProvider {

    constructor(private dbProvider: DatabaseProvider) { }

    public insert(client: Client) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            var sql = 'INSERT INTO DADF202 (CODIGO, RAZSOC, ENDERE, NUMERO, BAIRRO, CEPPPP, TELEFO, FAXXXX, TIPJUR, NUMCGC, NUMCPF, INSCRI, PARA01, PARA02) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

            var data = [
                client.CODIGO,
                client.RAZSOC,
                client.NUMERO,
                client.CEPPPP,
                client.TELEFO,
                client.FAXXXX,
                client.TIPJUR,
                client.NUMCGC,
                client.NUMCPF,
                client.INSCRI,
                "",
                "",
            ];

            return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public update(client: Client) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            let sql = 'UPDATE DAD202 SET CODIGO = ?, RAZSOC = ?, NUMERO = ?, CEPPPP = ?, TELEFO = ?, FAXXXX = ?, TIPJUR = ?, NUMCGC = ?, NUMCPF = ?, INSCRI = ?, PARA01 = ?, PARA02 = ? WHERE CODPRO = ?';

            let data = [
                client.CODIGO,
                client.RAZSOC,
                client.NUMERO,
                client.CEPPPP,
                client.TELEFO,
                client.FAXXXX,
                client.TIPJUR,
                client.NUMCGC,
                client.NUMCPF,
                client.INSCRI,
                client.PARA01,
                client.PARA02,
            ];

            return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public remove(codigo: number) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
            let sql  = 'DELETE FROM DAD202 WHERE CODIGO = ?';
            let data = [codigo];

            return db.executeSql(sql, data)
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public get(codigo: number) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            let sql  = 'SELECT * FROM DAD202 WHERE CODIGO = ?';
            let data = [codigo];

            return db.executeSql(sql, data)
            .then((data: any) => {
                if (data.rows.length > 0) {
                    let item       = data.rows.item(0);
                    let client    = new Client();

                    client.CODIGO = item.CODIGO;
                    client.RAZSOC = item.RAZSOC;
                    client.NUMERO = item.NUMERO;
                    client.BAIRRO = item.BAIRRO;
                    client.CEPPPP = item.CEPPPP;
                    client.TELEFO = item.TELEFO;
                    client.FAXXXX = item.FAXXXX;
                    client.TIPJUR = item.TIPJUR;
                    client.NUMCGC = item.NUMCGC;
                    client.NUMCPF = item.NUMCPF;
                    client.INSCRI = item.INSCRI;
                    client.PARA01 = item.PARA01;
                    client.PARA02 = item.PARA02;

                    return client;
                }

                return null;
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public getAll(razsoc: string = null) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            var sql = 'SELECT * FROM DADF202 ';
            var data: any[] = [];

            // filtrando pelo nome
            if(razsoc) {
                sql += ' WHERE RAZSOC LIKE ?'
                data.push('%' + razsoc + '%');
            }

            return db.executeSql(sql, data)
            .then((data: any) => {

                if (data.rows.length > 0) {
                    var clients: any[] = [];

                    for (var i = 0; i < data.rows.length; i++) {
                        var client = data.rows.item(i);
                        clients.push(client);
                    }
                    return clients;
                } else {
                    return [];
                }
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }
}

export class Client {
    CODIGO: number;
    RAZSOC: string;
    ENDERE: string;
    NUMERO: number;
    BAIRRO: number;
    CEPPPP: string;
    TELEFO: string;
    FAXXXX: string;
    TIPJUR: string;
    NUMCGC: string;
    NUMCPF: string;
    INSCRI: string;
    PARA01: string;
    PARA02: string;
}
