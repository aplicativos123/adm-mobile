import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../database/database';

@Injectable()
export class OrdersProvider {

    constructor(private dbProvider: DatabaseProvider) { }


    /*
    public get(codigo: number) {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            let sql  = 'SELECT * FROM DADF370 WHERE CODPRO = ?';
            let data = [codigo];

            return db.executeSql(sql, data)
            .then((data: any) => {
                if (data.rows.length > 0) {
                    let item       = data.rows.item(0);
                    let product    = new Product();

                    product.CODPRO = item.CODPRO;
                    product.DESPRO = item.DESPRO;
                    product.UNIDAD = item.UNIDAD;
                    product.IMGPRO = item.IMGPRO;
                    product.PRE001 = item.PRE001;

                    return product;
                }

                return null;
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }
    */

    public getAll() {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            var sql         = 'SELECT * FROM dadf109 ';
            var data: any[] = [];

            return db.executeSql(sql, data)
            .then((data: any) => {

                if (data.rows.length > 0) {
                    let payment: any[] = [];

                    for (var i = 0; i < data.rows.length; i++) {
                        var pay = data.rows.item(i);
                        payment.push(pay);
                    }
                    return payment;
                } else {
                    return [];
                }
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

    public insert() {
        return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {

            var sql         = 'SELECT * FROM dadf109 ';
            var data: any[] = [];

            return db.executeSql(sql, data)
            .then((data: any) => {

                if (data.rows.length > 0) {
                    let payment: any[] = [];

                    for (var i = 0; i < data.rows.length; i++) {
                        var pay = data.rows.item(i);
                        payment.push(pay);
                    }
                    return payment;
                } else {
                    return [];
                }
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }
}

export class Orders {
    codigo: number;
    descri: string;
    //duedate: Date;
    //active: boolean;
    //category_id: number;
}
