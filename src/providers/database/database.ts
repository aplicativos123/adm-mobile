import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ToastController, LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';

var syncURLClient  = 'http://200.98.69.63:8000/api-v2/Webservice/client';
var syncURLProdut  = 'http://200.98.69.63:8000/api-v2/Webservice/product';
var syncURLPayment = 'http://200.98.69.63:8000/api-v2/Webservice/payment';

@Injectable()
export class DatabaseProvider {

    dataSync: Observable<any>;

	constructor(private sqlite: SQLite, private http: HttpClient, private toast: ToastController, public loadingCtrl: LoadingController) { }

	/**
	 * create a database if no existing database with the name exists in the parameter
	 */
	public getDB() {
		return this.sqlite.create({
			name: 'adm-mobile.db',
			location: 'default'
		});
	}

	/**
	 * create the initial database structure
	 */
	public createDatabase() {
		return this.getDB()
        .then((db: SQLiteObject) => {
            this.createTables(db); // create database
		})
		.catch(e => console.log(e));
    }

    syncDatabase(token, codven) {

        var loader = this.loadingCtrl.create({
            content: "Integrando dados..."
        });
        loader.present();

        this.getDB()
        .then((db: SQLiteObject) => {

            this.deleteData(db);

            let headers = new HttpHeaders();
            headers.append('Access-Control-Allow-Origin' , '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept','application/json');
            headers.append('content-type','application/json');

            this.dataSync = this.http.get(syncURLClient+'/'+token+'/'+codven, {headers: headers});
            this.dataSync
            .subscribe(data => {

                var sql = 'INSERT INTO DADF202 (CODIGO, RAZSOC, ENDERE, NUMERO, BAIRRO, CEPPPP, TELEFO, FAXXXX, TIPJUR, NUMCGC, NUMCPF, INSCRI, PARA01, PARA02) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

                for (var i of data.data) {

                    db.executeSql(sql,
                        [i.codigo, i.razsoc, i.endere, i.numero, i.bairro, i.cepppp, i.telefo, i.faxxxx, i.tipjur, i.numcgc, i.numcgc, i.inscri, i.para01, i.para02]
                    )
                    .then(() => console.log('Cliente ' + i.razsoc + ' integrado'))
                    .catch(e => console.error('Erro ao incluir razsoc ' + i.razsoc, e));

                }
            });

            this.dataSync = this.http.get(syncURLProdut+'/'+token+'/'+codven, {headers: headers});
            this.dataSync
            .subscribe(data => {

                var sql = 'INSERT INTO DADF370 (CODPAU, CODPRO, DESPAU, DESPRO, UNIDAD, IMGPRO, CON001, DES001, DIA001, PRE001, CON002, DES002, DIA002, PRE002, CON003, DES003, DES003, DIA003, PRE003, CON004, DES004, DIA004, PRE004, CON005, DES005, DIA005, PRE005, CON006, DES006, DIA006, PRE006, CON007, DES007, DIA007, PRE007, CON008, DES008, DIA008, PRE008, CON009, DES009, DIA009, PRE009, CON010, DES010, DIA010, PRE010, PARA01, PARA02, PARA03, PARA04, PARA05, REST01, REST02, REST03, RESL01, RESL02, RESL03, RESD01, RESD02, RESD03) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

                for (var i of data.data) {

                    db.executeSql(sql,
                        [i.CODPAU, i.CODPRO, i.DESPAU, i.DESPRO, i.UNIDAD, i.imgpro, i.CON001, i.DES001, i.DIA001, i.PRE001, i.CON002, i.DES002, i.DIA002, i.PRE002,  i.CON003, i.DES003, i.DIA003, i.PRE003]
                    )
                    .then(() => console.log('Produto ' + i.DESPRO + ' integrado'))
                    .catch(e => console.error('Erro ao incluir produto ' + i.DESPRO, e));
                }
            });

            this.dataSync = this.http.get(syncURLPayment+'/'+token+'/'+codven, {headers: headers});
            this.dataSync
            .subscribe(data => {

                var sql = 'INSERT INTO dadf109 (CODIGO, DESCRI, TIPOCO, TAXMAX, INDACR, TIPIND, VALENT, NUMEVE, PRAZ01, PRAZ02, PRAZ03, PRAZ04, PRAZ05, PRAZ06, PRAZ07, PRAZ08, PRAZ09, PRAZ10, PRAZ11, PRAZ12, PRAZ13, PRAZ14, PRAZ15, PRAZ16, PRAZ17, PRAZ18, PRAZ19, PRAZ20, PRAZ21, PRAZ22, PRAZ23, PRAZ24, PRAZ25, PRAZ26, PRAZ27, PRAZ28, PRAZ29, PRAZ30, PRAZ31, PRAZ32, PRAZ33, PRAZ34, PRAZ35, PRAZ36, PARA01, PARA02, PARA03, PARA04, PARA05, PARA06, PARA07, PARA08, PARA09, PARA10, PARA11, PARA12, PARA13, PARA14, PARA15, PARA16, PARA17, PARA18, PARA19, PARA20, PERCOM, PERCO2, RESL01, RESL02, RESL03, RESL04, RESL05, REST01, REST02, REST03, REST04, REST05, RESC01, RESC02, RESC03, RESC04, RESC05, RESD01, RESD02, RESD03, RESD04, RESD05 ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';

                for (var i of data.data) {

                    db.executeSql(sql,
                        [i.CODIGO, i.DESCRI, i.TIPOCO, i.TAXMAX, i.INDACR, i.TIPIND, i.VALENT, i.NUMEVE, i.PRAZ01, i.PRAZ02, i.PRAZ03, i.PRAZ04, i.PRAZ05, i.PRAZ06, i.PRAZ07, i.PRAZ08, i.PRAZ09, i.PRAZ10, i.PRAZ11, i.PRAZ12, i.PRAZ13, i.PRAZ14, i.PRAZ15, i.PRAZ16, i.PRAZ17, i.PRAZ18, i.PRAZ19, i.PRAZ20, i.PRAZ21, i.PRAZ22, i.PRAZ23, i.PRAZ24, i.PRAZ25, i.PRAZ26, i.PRAZ27, i.PRAZ28, i.PRAZ29, i.PRAZ30, i.PRAZ31, i.PRAZ32, i.PRAZ33, i.PRAZ34, i.PRAZ35, i.PRAZ36, i.PARA01, i.PARA02, i.PARA03, i.PARA04, i.PARA05, i.PARA06, i.PARA07, i.PARA08, i.PARA09, i.PARA10, i.PARA11, i.PARA12, i.PARA13, i.PARA14, i.PARA15, i.PARA16, i.PARA17, i.PARA18, i.PARA19, i.PARA20, i.PERCOM, i.PERCO2, i.RESL01, i.RESL02, i.RESL03, i.RESL04, i.RESL05, i.REST01, i.REST02, i.REST03, i.REST04, i.REST05, i.RESC01, i.RESC02, i.RESC03, i.RESC04, i.RESC05, i.RESD01, i.RESD02, i.RESD03, i.RESD04, i.RESD05]
                    )
                    .then(() => console.log('Forma de pagamento ' + i.DESCRI + ' integrado'))
                    .catch(e => console.error('Erro ao incluir Forma de pagamento ' + i.DESCRI, e));
                }
            });

            setTimeout(function () {
                loader.dismiss();
            }, 10000);
        });
    }

	/**
	 * creating tables in database
	 * @param db
	 */
	private createTables(db: SQLiteObject) {

        var tableDADF370 = 'CREATE TABLE IF NOT EXISTS DADF370 ( ';
        tableDADF370 += ' CODPAU INT DEFAULT 0, ' ,
        tableDADF370 += ' CODPRO VARCHAR(20) DEFAULT "", ' ,
        tableDADF370 += ' DESPAU VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DESPRO VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' UNIDAD VARCHAR(2) DEFAULT "", ' ,
        tableDADF370 += ' IMGPRO VARCHAR(150) DEFAULT "assets/img/produtos/no-image.png", ' ,
        tableDADF370 += ' CON001 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES001 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA001 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE001 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON002 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES002 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA002 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE002 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON003 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES003 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA003 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE003 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON004 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES004 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA004 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE004 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON005 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES005 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA005 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE005 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON006 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES006 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA006 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE006 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON007 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES007 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA007 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE007 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON008 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES008 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA008 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE008 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON009 INT DEFAULT NULL, ' ,
        tableDADF370 += ' DES009 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA009 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE009 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' CON010 INT DEFAULT 0, ' ,
        tableDADF370 += ' DES010 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' DIA010 INT DEFAULT 0, ' ,
        tableDADF370 += ' PRE010 FLOAT(19,4) DEFAULT 0, ' ,
        tableDADF370 += ' PARA01 VARCHAR(2) DEFAULT "", ' ,
        tableDADF370 += ' PARA02 VARCHAR(2) DEFAULT "", ' ,
        tableDADF370 += ' PARA03 VARCHAR(2) DEFAULT "", ' ,
        tableDADF370 += ' PARA04 VARCHAR(2) DEFAULT "", ' ,
        tableDADF370 += ' PARA05 VARCHAR(2) DEFAULT "", ' ,
        tableDADF370 += ' REST01 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' REST02 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' REST03 VARCHAR(50) DEFAULT "", ' ,
        tableDADF370 += ' RESC01 INT DEFAULT 0, ' ,
        tableDADF370 += ' RESC02 INT DEFAULT 0, ' ,
        tableDADF370 += ' RESC03 INT DEFAULT 0, ' ,
        tableDADF370 += ' RESL01 INT DEFAULT 0, ' ,
        tableDADF370 += ' RESL02 INT DEFAULT 0, ' ,
        tableDADF370 += ' RESL03 INT DEFAULT 0, ' ,
        tableDADF370 += ' RESD01 DATETIME DEFAULT CURRENT_TIMESTAMP, ' ,
        tableDADF370 += ' RESD02 DATETIME DEFAULT CURRENT_TIMESTAMP, ' ,
        tableDADF370 += ' RESD03 DATETIME DEFAULT CURRENT_TIMESTAMP ' ,
        tableDADF370 += ');';

        var tableDADF202 = 'CREATE TABLE IF NOT EXISTS DADF202 (';
        tableDADF202 += 'CODIGO	VARCHAR(50) DEFAULT "", ',
        tableDADF202 += 'RAZSOC	VARCHAR(50) DEFAULT "", ',
        tableDADF202 += 'ENDERE	VARCHAR(50) DEFAULT "", ',
        tableDADF202 += 'NUMERO	INT DEFAULT 0, ',
        tableDADF202 += 'BAIRRO	VARCHAR(25) DEFAULT "", ',
        tableDADF202 += 'CEPPPP	INT DEFAULT "", ',
        tableDADF202 += 'TELEFO	VARCHAR(15) DEFAULT "", ',
        tableDADF202 += 'FAXXXX	VARCHAR(15) DEFAULT "", ',
        tableDADF202 += 'TIPJUR	VARCHAR(1) DEFAULT "", ',
        tableDADF202 += 'NUMCGC	VARCHAR(14) DEFAULT "", ',
        tableDADF202 += 'NUMCPF	VARCHAR(11) DEFAULT "", ',
        tableDADF202 += 'INSCRI	VARCHAR(20) DEFAULT "", ',
        tableDADF202 += 'PARA01	VARCHAR(2) DEFAULT "", ',
        tableDADF202 += 'PARA02	VARCHAR(2) DEFAULT ""',
        tableDADF202 += ') ';

        var tableDADP334 = 'CREATE TABLE IF NOT EXISTS dadp334 (';
        tableDADP334 += ' numped INTEGER PRIMARY KEY NOT NULL, ';
        tableDADP334 += ' seqped INT NOT NULL DEFAULT 1, ';
        tableDADP334 += ' codcli INT DEFAULT 0, ';
        tableDADP334 += ' codtra INT DEFAULT 0, ';
        tableDADP334 += ' conpag INT DEFAULT 0, ';
        tableDADP334 += ' coco01 INT DEFAULT 0, ';
        tableDADP334 += ' coco02 INT DEFAULT 0, ';
        tableDADP334 += ' codven INT DEFAULT 0, ';
        tableDADP334 += ' codve2 INT DEFAULT 0, ';
        tableDADP334 += ' percom FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' datlan DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADP334 += ' datemi DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADP334 += ' prefat DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADP334 += ' compri FLOAT(0,0) DEFAULT 0, ';
        tableDADP334 += ' largur FLOAT(0,0) DEFAULT 0, ';
        tableDADP334 += ' espess FLOAT(0,0) DEFAULT 0, ';
        tableDADP334 += ' pedrep VARCHAR(10) DEFAULT "", ';
        tableDADP334 += ' totpro FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' totdes FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' totped FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' basipi FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' valipi FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' valfre FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' valost FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' mens01 VARCHAR(200) DEFAULT "", ';
        tableDADP334 += ' status INT DEFAULT 1, ';
        tableDADP334 += ' observ VARCHAR(200) DEFAULT "", ';
        tableDADP334 += ' valent FLOAT(19,4) DEFAULT 0, ';
        tableDADP334 += ' datval DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADP334 += ' pedpag VARCHAR(1) DEFAULT "", ';
        tableDADP334 += ' para01 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para02 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para03 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para04 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para05 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para06 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para07 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para08 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para09 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' para10 VARCHAR(2) DEFAULT "", ';
        tableDADP334 += ' pedcli VARCHAR(10) DEFAULT "" ';
        tableDADP334 += ' );';

        var tableDADP335 = 'CREATE TABLE IF NOT EXISTS dadp335 ( ';
        tableDADP335 += ' numped INT NOT NULL, ';
        tableDADP335 += ' seqped INT DEFAULT 1, ';
        tableDADP335 += ' seqpro INT DEFAULT 1, ';
        tableDADP335 += ' produt VARCHAR(20) DEFAULT "", ';
        tableDADP335 += ' quanti FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' valuni FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' valtot FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' aliipi FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' valipi FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' indist FLOAT(19,8) DEFAULT 0, ';
        tableDADP335 += ' valost FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' perdes FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' valdes FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' compri FLOAT(0,0) DEFAULT 0, ';
        tableDADP335 += ' largur FLOAT(0,0) DEFAULT 0, ';
        tableDADP335 += ' espess FLOAT(0,0) DEFAULT 0, ';
        tableDADP335 += ' para01 VARCHAR(2) DEFAULT "", ';
        tableDADP335 += ' para02 VARCHAR(2) DEFAULT "", ';
        tableDADP335 += ' para03 VARCHAR(2) DEFAULT "", ';
        tableDADP335 += ' para04 VARCHAR(2) DEFAULT "", ';
        tableDADP335 += ' para05 VARCHAR(2) DEFAULT "", ';
        tableDADP335 += ' rest01 VARCHAR(50) DEFAULT "", ';
        tableDADP335 += ' rest02 VARCHAR(50) DEFAULT "", ';
        tableDADP335 += ' resd01 DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADP335 += ' resd02 DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADP335 += ' resc01 FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' resc02 FLOAT(19,4) DEFAULT 0, ';
        tableDADP335 += ' resl01 INT DEFAULT 0, ';
        tableDADP335 += ' resl02 INT DEFAULT 0 ';
        tableDADP335 += ' );';

        var tableDADF109 = 'CREATE TABLE IF NOT EXISTS dadf109 ( ';
        tableDADF109 += '    CODIGO  INT NOT NULL, ';
        tableDADF109 += '    DESCRI  VARCHAR(50) DEFAULT "", ';
        tableDADF109 += '    TIPOCO  VARCHAR(1) DEFAULT "", ';
        tableDADF109 += '    TAXMAX  FLOAT(0,0) DEFAULT 0, ';
        tableDADF109 += '    INDACR  FLOAT(0,0) DEFAULT 0, ';
        tableDADF109 += '    TIPIND  VARCHAR(1) DEFAULT "", ';
        tableDADF109 += '    VALENT  VARCHAR(1) DEFAULT "", ';
        tableDADF109 += '    NUMEVE  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ01  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ02  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ04  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ03  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ05  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ06  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ07  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ08  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ09  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ10  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ11  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ12  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ13  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ14  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ15  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ16  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ17  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ18  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ19  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ20  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ21  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ22  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ23  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ24  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ25  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ26  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ27  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ28  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ29  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ30  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ31  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ32  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ33  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ34  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ35  INT DEFAULT 0, ';
        tableDADF109 += '    PRAZ36  INT DEFAULT 0, ';
        tableDADF109 += '    PARA01  VARCHAR(2) DEFAULT "", ';
        tableDADF109 += '    PARA02  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA03  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA04  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA05  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA06  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA07  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA08  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA09  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA10  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA11  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA12  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA13  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA14  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA15  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA16  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA17  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA18  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA19  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PARA20  VARCHAR(2) DEFAULT NULL, ';
        tableDADF109 += '    PERCOM  FLOAT(19,4) DEFAULT 0, ';
        tableDADF109 += '    PERCO2  FLOAT(19,4) DEFAULT 0, ';
        tableDADF109 += '    RESL01  INT DEFAULT 0, ';
        tableDADF109 += '    RESL02  INT DEFAULT 0, ';
        tableDADF109 += '    RESL03  INT DEFAULT 0, ';
        tableDADF109 += '    RESL04  INT DEFAULT 0, ';
        tableDADF109 += '    RESL05  INT DEFAULT 0, ';
        tableDADF109 += '    REST01  VARCHAR(50) DEFAULT "", ';
        tableDADF109 += '    REST02  VARCHAR(50) DEFAULT "", ';
        tableDADF109 += '    REST03  VARCHAR(50) DEFAULT "", ';
        tableDADF109 += '    REST04  VARCHAR(50) DEFAULT "", ';
        tableDADF109 += '    REST05  VARCHAR(50) DEFAULT "", ';
        tableDADF109 += '    RESC01  FLOAT(19,4) DEFAULT 0, ';
        tableDADF109 += '    RESC02  FLOAT(19,4) DEFAULT 0, ';
        tableDADF109 += '    RESC03  FLOAT(19,4) DEFAULT 0, ';
        tableDADF109 += '    RESC04  FLOAT(19,4) DEFAULT 0, ';
        tableDADF109 += '    RESC05  FLOAT(19,4) DEFAULT 0, ';
        tableDADF109 += '    RESD01  DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADF109 += '    RESD02  DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADF109 += '    RESD03  DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADF109 += '    RESD04  DATETIME DEFAULT CURRENT_TIMESTAMP, ';
        tableDADF109 += '    RESD05  DATETIME DEFAULT CURRENT_TIMESTAMP ';
        tableDADF109 += ' ); ';

		db.sqlBatch([
            [tableDADF370],
            [tableDADF202],
            [tableDADP334],
            [tableDADP335],
            [tableDADF109]
		])
		.then(() => console.log('Tabelas criadas'))
		.catch(e => console.error('Erro ao criar tabelas ', e));
	}

    private deleteData(db: SQLiteObject) {

        var SQL1 = 'DELETE FROM DADF370';
        var SQL2 = 'DELETE FROM DADF202';
        var SQL3 = 'DELETE FROM DADF109';

        db.executeSql(SQL1, [])
		.then(() => console.log('Limpando dados DADF370'))
		.catch(e => console.error('Erro ao limpar dados DADF370', e));

        db.executeSql(SQL2, [])
		.then(() => console.log('Limpando dados DADF202'))
        .catch(e => console.error('Erro ao limpar dados DADF370', e));

        db.executeSql(SQL3, [])
		.then(() => console.log('Limpando dados DADF109'))
        .catch(e => console.error('Erro ao limpar dados DADF109', e));
    }

    /*private insertDataDADF202(db: SQLiteObject) {
        var SQL  = 'SELECT COUNT(CODIGO) AS qtd FROM DADF202';

        db.executeSql(SQL, [])
		.then((data: any) => {

            // if there is no record
			if (data.rows.item(0).qtd <= 0) {
				db.sqlBatch([
                    ['INSERT INTO DADF202 (CODIGO, RAZSOC, ENDERE, NUMERO, BAIRRO, CEPPPP, TELEFO, FAXXXX, TIPJUR, NUMCGC, NUMCPF, INSCRI, PARA01, PARA02) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                    ["1", "ADM INFORMATICA", "RUA TESTE", "1200", "CENTRO", "89300000", "4736426833", "4736426833", "1", "005523150000158", "", "", "", ""]]
				])
				.then(() => console.log('Cliente padrao inserido '))
				.catch(e => console.error('Erro ao incluir cliente padrao ', e));

			}
		})
		.catch(e => console.error('Erro ao consultar a quantidade de produtos', e));
	}

	private insertDataDADF370(db: SQLiteObject) {
        var SQL  = 'SELECT COUNT(CODPRO) AS qtd FROM DADF370';

        db.executeSql(SQL, [])
		.then((data: any) => {

            // if there is no record
			if (data.rows.item(0).qtd <= 0) {
				db.sqlBatch([
                    ['INSERT INTO DADF370 (CODPAU, CODPRO, DESPAU, DESPRO, UNIDAD, IMGPRO, CON001, DES001, DIA001, PRE001, CON002, DES002, DIA002, PRE002, CON003, DES003, DES003, DIA003, PRE003, CON004, DES004, DIA004, PRE004, CON005, DES005, DIA005, PRE005, CON006, DES006, DIA006, PRE006, CON007, DES007, DIA007, PRE007, CON008, DES008, DIA008, PRE008, CON009, DES009, DIA009, PRE009, CON010, DES010, DIA010, PRE010, PARA01, PARA02, PARA03, PARA04, PARA05, REST01, REST02, REST03, RESL01, RESL02, RESL03, RESD01, RESD02, RESD03) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                    ['1', '1', '1', 'Produto Padrao', 'UN', '', '0', '', '0', '150', ' ', '0', '0', ' ', '0', '0', ' ', '0', '0', ' ', '0', '0', ' ', '0', '0', '0', '0', '0', '0', '0', '0', '0', ' ', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '0', '0', '0', '0', '0', '0', '', '', '']]
				])
				.then(() => console.log('Produto padrao incluso'))
				.catch(e => console.error('Erro ao incluir produto padrao ', e));
			}
		})
		.catch(e => console.error('Erro ao consultar a quantidade de produtos', e));
	}*/
}
