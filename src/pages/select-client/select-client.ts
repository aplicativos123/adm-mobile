import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ClientProvider } from '../../providers/client/client';

@IonicPage()
@Component({
    selector: 'page-select-client',
    templateUrl: 'select-client.html',
})
export class SelectClientPage {

    clients:      any[]  = [];
    searchClient: string = null;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private clientProvider: ClientProvider,
        public alertCtrl: AlertController) {
    }


    ionViewDidEnter() {
        this.getAllClients();
    }

    ionViewDidLoad() {

    }

    getAllClients() {
        this.clientProvider.getAll(this.searchClient)
        .then((result: any[]) => {
            this.clients = result;
        });
    }

    filterClients(ev: any) {
        this.getAllClients();
    }

    selectClient(codigo, razsoc) {
        if(codigo != '' && razsoc != '') {
            this.navCtrl.push('OrdersPage', {
                codigo: codigo,
                razsoc: razsoc
            });
        } else {
            this.alertCtrl.create({
                title: 'Nenhum cliente foi selecionado!',
                subTitle: 'Por favor verifique!' ,
                buttons: ['OK']
            }).present();
        }
    }
}
