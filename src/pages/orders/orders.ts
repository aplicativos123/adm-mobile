import { OrdersProvider, Orders } from '../../providers/orders/orders';
import { ClientProvider, Client } from '../../providers/client/client';
import { Storage } from '@ionic/storage';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,  ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

    payment:  any[]     = [];
    clients:  any[]     = [];
    itens:    any[]     = [];
    qtdItens: any       = 0;
    valTotal: any       = 0;
    totalST:  any       = 0;
    totalIPI: any       = 0;
    shownGroup: string  = null;

    nomusu: string;
    codven: string;
    codigo: string;
    razsoc: string;
    dateNow: string = new Date().toISOString();

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private ordersProvider: OrdersProvider,
        private clientProvider: ClientProvider,
        public alertCtrl: AlertController,
        private storage: Storage,
        public modalCtrl: ModalController) {
    }

    ionViewDidLoad() {
        this.getPayment();
        this.getAllClients();
        this.qtdItens = 0;
        this.valTotal = 0;
        this.totalST = 0;
        this.totalIPI = 0;
        this.codigo   = this.navParams.get('codigo');
        this.razsoc   = this.navParams.get('razsoc');

        this.storage.get('user').then(user => {
            this.nomusu = user.nomusu;
            this.codven = user.codven;
        });
    }

    getPayment() {
        this.ordersProvider.getAll()
        .then((result: any[]) => {
            this.payment = result;
        });
    }

    getAllClients() {
        this.clientProvider.getAll('')
        .then((result: any[]) => {
            this.clients = result;
        });
    }

    selectProduct() {
        let profileModal = this.modalCtrl.create("SelectProductPage");
        
        /* essa função é chamada quando é dado dismiss() no modal "SelectProductPage"  */
        profileModal.onDidDismiss(data => {
            console.log("data " + data);
            console.log('qtde itens: ' + data.totalItens);
            
           /*  incrementa a quantidade e os valores com os dados do retorno do modal "SelectProductPage" */
            this.qtdItens += data.totalItens;
            this.valTotal += data.totalValor;
            this.totalST += data.totalST;
            this.totalIPI += data.totalIPI;

            /* é preciso adicionar os itens do retorno do modal no this.itens sugiro fazer um for e ir dando itens.push */
            for (let item of data.itens){
                this.itens.push(item);
            }
        });
        
        profileModal.present();
    }

    selectClient() {
        this.navCtrl.setRoot("SelectClientPage");
    }

    click() {
        this.alertCtrl.create({
            title: 'Pedido salvo!',
            subTitle: '',
            buttons: ['OK']
        }).present();
    }

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    };

    isGroupShown(group) {
        return this.shownGroup === group;
    };
}
