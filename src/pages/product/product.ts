import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ProductProvider, Product } from '../../providers/product/product';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {

    products: any[]    = [];
    searchText: string = null;
    private page: number = 0;

	constructor(public navCtrl: NavController, private toast: ToastController, private productProvider: ProductProvider) { }

	ionViewDidEnter() {
		this.getAllProducts();
	}

	/*openLogin() {
  		this.navCtrl.push('LoginPage');
	}*/

	getAllProducts() {
		this.productProvider.getAll(this.searchText)
		.then((result: any[]) => {
            this.products = result;
		});
	}

	/*addProduct() {
		this.navCtrl.push('EditProductPage');
	}

	editProduct(id: number) {
		this.navCtrl.push('EditProductPage', { id: id });
	}

	removeProduct(product: Product) {
		this.productProvider.remove(product.CODPRO)
		  .then(() => {
			// Removendo do array de produtos
			var index = this.products.indexOf(product);
			this.products.splice(index, 1);

            this.toast.create({
                message: 'Produto removido!',
                duration: 3000,
                position: 'botton'
            }).present();
		})
	}*/

  	filterProducts(ev: any) {
    	this.getAllProducts();
    }

    doInfinite(infiniteScroll: any) {
        this.page++;
        this.getAllProducts();
        setTimeout(() => {
            infiniteScroll.complete();
        }, 1000);
    }

    doRefresh(refresher) {
        this.page = 0;
        this.products = [];
        this.getAllProducts();
        setTimeout(() => {
            refresher.complete();
        }, 1000);
    }

    reset() {
        this.products = this.products;
    }
}
