import { ClientProvider, Client } from '../../providers/client/client';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-client',
    templateUrl: 'client.html',
})
export class ClientPage {

    clients:      any[] = [];
    searchClient: string = null;
    shownGroup:   string = null;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private clientProvider: ClientProvider,
        public alertCtrl: AlertController
    ) { }

    ionViewDidLoad() {
    }

    ionViewDidEnter() {
        this.getAllClients();
	}

    getAllClients() {
        this.clientProvider.getAll(this.searchClient)
        .then((result: any[]) => {
            this.clients = result;
        });
    }

    addClient() {
		this.navCtrl.push('ClientPage');
	}

	/*editClient(id: number) {
		this.navCtrl.push('ClientPage', { id: id });
	}

	removeClient(client: Client) {
		this.clientProvider.remove(client.CODIGO)
		  .then(() => {

			var index = this.clients.indexOf(client);
			this.clients.splice(index, 1);

            this.toast.create({
                message: 'Produto removido!',
                duration: 3000,
                position: 'botton'
            }).present();
		})
	}*/

    filterClients(ev: any) {
    	this.getAllClients();
    }

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    };

    isGroupShown(group) {
        return this.shownGroup === group;
    };
}
