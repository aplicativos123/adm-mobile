import { ProductProvider } from '../../providers/product/product';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Product } from '../../providers/product/product';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@IonicPage()
@Component({
  selector: 'page-select-product',
  templateUrl: 'select-product.html',
})
export class SelectProductPage {
    model: Product;
    products: any[]    = [];
    searchText: string = null;

    totalItens: any = 0;
    totalValor: any = 0;
    totalST:    any = 0;
    totalIPI:   any = 0;
    totalDesc:  any = 0;

    constructor(
        public viewController: ViewController,
        public navParams: NavParams,
        private productProvider: ProductProvider,
        public alertCtrl: AlertController)
        {

        this.model = new Product();
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad SelectProductPage');
        this.getAllProducts();
        this.sumQuanti('somar');
    }

    getAllProducts() {
		this.productProvider.getAll(this.searchText)
		.then((result: any[]) => {
            this.products = result;
		});
    }

    increment(item) {
        item.CON010 += 1;
        //item.DES009 += item.PRE001 * item.CON010 * 1.5;
        //item.DES010 += item.PRE001 * item.CON010 * 10 / 100;
    }

    decrement(item) {
        if(item.CON010 > 0) {
            item.CON010 -= 1;
            //item.DES009 -= item.PRE001 * item.CON010 * 1.5 -1;
            //item.DES010 -= item.PRE001 * item.CON010 * 10 / 100 * -1;
        }
    }

    sumQuanti(tipo) {
        var sum = 0;

        this.products.forEach(function(item, index) {

            var quanti = parseInt(item.CON010, 10);
            if(isNaN(quanti)) quanti = 0;

            if(tipo == 'somar') {
                sum += quanti;
            } else {
                sum -= quanti * -1;
            }
        });

        this.totalItens = sum;

        return this.totalItens;
    };

    valorTotal(tipo) {
        var total = 0;

        this.products.forEach(function(item, index) {

            var quanti = parseInt(item.CON010, 10);
            if(isNaN(quanti)) quanti = 0;

            if(tipo == 'somar') {
                total += item.PRE001 * quanti;
            } else {
                total -= item.PRE001 * quanti * -1;
            }
        });

        this.totalValor = total;

        return this.totalValor;
    }

    valorST(tipo) {
        var st = 0;

        this.products.forEach(function(item, index) {

            var quanti = parseInt(item.CON010, 10);
            if(isNaN(quanti)) quanti = 0;

            if(tipo == 'somar') {
                st += item.PRE001 * quanti * 1.5;
            } else {
                st -= item.PRE001 * quanti * 1.5 * -1;
            }

        });

        this.totalST = st;

        return this.totalST;
    }

    valorIPI(tipo) {
        var ipi = 0;

        this.products.forEach(function(item, index) {

            var quanti = parseInt(item.CON010, 10);
            if(isNaN(quanti)) quanti = 0;

            if(tipo == 'somar') {
                ipi += item.PRE001 * quanti * 10 / 100;
            } else {
                ipi -= item.PRE001 * quanti * 10 / 100 * -1;
            }
        });

        this.totalIPI = ipi;

        return this.totalIPI;
    }

    filterProducts(ev: any) {
    	this.getAllProducts();
    }

    scanProduct() {
    /*    this.barcodeScanner.scan().then(barcodeData => {
            console.log(JSON.stringify(barcodeData))
        }).catch(err => {
            console.log('Error', err);
        });*/
    }

    saveItens() {
        var itens: any[] = [];

        for (var i = 0; i < this.products.length; i++) {
            var item = this.products[i];

            if(this.products[i].CON010 > 0) {
                itens.push(item);
            }
        }

        if(itens.length > 0) {
            console.log(this.totalItens);

            this.viewController.dismiss({
                totalItens: this.totalItens,
                totalValor: this.totalValor,
                totalST: this.totalST,
                totalIPI: this.totalIPI,
                itensList: itens
            });
        } else {
            this.alertCtrl.create({
                title: 'Nenhum produto foi selecionado!',
                subTitle: 'Por favor verifique!' ,
                buttons: ['OK']
            }).present();
        }

    }
}
