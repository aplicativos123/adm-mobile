
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IonicPage, MenuController, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HomePage } from '../home/home';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { Md5 } from 'ts-md5/dist/md5';

import { DatabaseProvider } from './../../providers/database/database';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

var apiUrl = 'http://200.98.69.63:8000/api-v2/Webservice/authentication/';

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {

	form: FormGroup;
    email: string;
    password: string;
    api:  Observable<any>;

	constructor(
        public navCtrl: NavController,
        private menu: MenuController,
        public navParams: NavParams,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private dbProvider: DatabaseProvider,
        public alertCtrl: AlertController,
        public storage: Storage,
        private network: Network) {

		this.form = formBuilder.group({
            email: ["", Validators.required],
            password: ["", Validators.required]
        });

        this.form = new FormGroup({
            email: new FormControl(),
            password: new FormControl()
        });
    }

	signIn() {
        //var headers = new HttpHeaders();
        //headers     = headers.set('Content-Type', 'application/json; charset=utf-8');

        let headers = new HttpHeaders();
        headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
        headers.append('Accept','application/json');
        headers.append('content-type','application/json');

        if(this.email != undefined && this.password != undefined) {
            var params = {
                email: this.email,
                password: Md5.hashStr(this.password)
            };

            this.http.post(apiUrl, params, {headers: headers})
            .subscribe(data => {
                if(data['code'] == '404') {
                    this.alertCtrl.create({
                        title: data['data'],
                        subTitle: 'Code: ' + data['code'] ,
                        buttons: ['OK']
                    }).present();

                } else if(data['code'] == '403') {
                    this.alertCtrl.create({
                        title: data['data'],
                        subTitle: 'Code: ' + data['code'] ,
                        buttons: ['OK']
                    }).present();

                } else if(data['code'] == '400') {
                    this.alertCtrl.create({
                        title: data['data'],
                        subTitle: 'Code: ' + data['code'],
                        buttons: ['OK']
                    }).present();
                } else if(data['code'] == '200') {
                    let user = {
                        token: data['data']['tokusu'],
                        codven: data['data']['codven'],
                        nomusu: data['data']['nomusu']
                    };

                    this.storage.set('user', user);

                    //this.network.onConnect().subscribe(() => {
                        //this.dbProvider.syncDatabase(token, codven);
                    //});

                    this.navCtrl.push(HomePage);
                }
            });
        } else {
            this.alertCtrl.create({
                title: 'Falha na autenticação',
                subTitle: 'Nenhum e-mail e senha foram informados!',
                buttons: ['OK']
            }).present();
        }
    }

    logout() {
        this.navCtrl.setRoot(LoginPage);
        this.menu.enable(false);
        localStorage.clear();
    }

    ionViewDidEnter() {
        // Disable the root left menu when entering this page
        this.menu.enable(false);
    }

    ionViewWillLeave() {
        // enable the root left menu when leaving this page
        this.menu.enable(true);
    }
}
