import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
registerLocaleData(localePt);

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProductPage } from '../pages/product/product';
import { ClientPage } from '../pages/client/client';
import { OrdersPage } from '../pages/orders/orders';
import { ReportsPage } from '../pages/reports/reports';
import { SelectProductPage } from '../pages/select-product/select-product';

import { SQLite } from '@ionic-native/sqlite'
import { DatabaseProvider } from '../providers/database/database';
import { ProductProvider } from '../providers/product/product';
import { Network } from '@ionic-native/network';
import { HttpClientModule } from '@angular/common/http';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { ClientProvider } from '../providers/client/client';
import { IonicStorageModule } from '@ionic/storage';
import { OrdersProvider } from '../providers/orders/orders';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        LoginPage,
        ProductPage,
        ClientPage,
        ReportsPage
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        LoginPage,
        ProductPage,
        ClientPage,
        ReportsPage
    ],
    providers: [
        StatusBar,
        Network,
        SplashScreen,
        //BarcodeScanner,
        {
            provide: LOCALE_ID,
            useValue: 'pt-BR'
        }, // formatar numeros e datas pt
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        },
        SQLite,
        DatabaseProvider,
        ProductProvider,
        AuthServiceProvider,
        ClientProvider,
        OrdersProvider
    ]
})
export class AppModule {}
