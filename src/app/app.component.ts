
import { Storage } from '@ionic/storage';
/* import others libraries */
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
//import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

/* import providers */
import { DatabaseProvider } from '../providers/database/database';

/* import pages */
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProductPage } from '../pages/product/product';
import { ClientPage } from '../pages/client/client';
import { ReportsPage } from '../pages/reports/reports';
import { OrdersPage } from '../pages/orders/orders';
import { SelectProductPage } from '../pages/select-product/select-product';

@Component({
  	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav; Nav;
	rootPage: any = null;
	pages: Array<{title: string, component: any, icon: any}>;
    token: string;
    codven: string;
    link: string;

	constructor(
        platform: Platform,
        statusBar: StatusBar,
        splashScreen: SplashScreen,
        private dbProvider: DatabaseProvider,
        private storage: Storage,
        app: App) {
		platform.ready().then(() => {
			statusBar.styleDefault();

			//create the database
			dbProvider.createDatabase()
			.then(() => {
				this.openLoginPage(splashScreen); // closes the splash only when the database is created
			})
			.catch(() => {
				this.openLoginPage(splashScreen);  // or if there is an error in creating the database
			});

            platform.registerBackButtonAction(() => {
                app.navPop();
            });
		});

        this.pages = [
            { title: 'Pedidos', component: HomePage, icon: 'document'},
            { title: 'Clientes', component: ClientPage, icon: 'person'},
            { title: 'Produtos', component: ProductPage, icon: 'podium'},
            { title: 'Relatórios', component: ReportsPage, icon: 'pie'},
            { title: 'Sistema Online', component: null, icon: 'cloud'},
            { title: 'Sincronizar', component: null, icon: 'sync'},
            { title: 'Sair', component: null, icon: 'power'}
        ];
  	}

  	//private openHomePage(splashScreen: SplashScreen) {
	private openLoginPage(splashScreen: SplashScreen) {
        splashScreen.hide();
        this.rootPage = LoginPage;
        //this.rootPage = SelectProductPage;
    }

	openPage(page) {

        switch (true) {
            case ((page.title == 'Sair')): {
                this.nav.setRoot(LoginPage);
            } break;

            case ((page.title == 'Sincronizar')): {
                this.storage.get('user').then(user => {
                    this.dbProvider.syncDatabase(user.token, user.codven);
                });

                this.nav.setRoot(HomePage);
            } break;

            case ((page.title == 'Sistema Online')): {
                window.open("http://200.98.69.63:8000/api-v2/", '_system', 'location=yes');
            } break;

            default: {
                this.nav.setRoot(page.component);
            } break;
        }
    }
}
